import pytest


@pytest.fixture(scope='function', autouse=True)
def package_2_fixture():
    print('\nFunction Test is started')
    yield
    print('\nFunction Test is finished')