# Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на "о"
# (враховуються як великі так і маленькі).

# приклад для вводу -> Oreo, Ocean, is the mostO beautiful placesO in! the worldo trueO? укро укрО укро! укрО!

inputted_text = input('Введи текст, а я пiдрахую скiльки слiв у ньому закiнчується на \'o\' ---> ')
inputted_text = inputted_text.lower()           #переводжу в нижній регістр весь текст
inputted_text = inputted_text.replace('!', '')
inputted_text = inputted_text.replace('?', '')
inputted_text = inputted_text.replace(')', '')
inputted_text = inputted_text.replace('(', '')
inputted_text = inputted_text.replace(':', '')
inputted_text = inputted_text.replace('.', '')
inputted_text = inputted_text.replace(',', '')  #прибираю деякi можливі спецсимволи які можуть бути після букви "о" в кінці слова
outputted_text = []                             #нова колекція в яку додаватимуться слова з "о" в кінці
formatted_text = inputted_text.split()          #роблю колекцію зі стрінга
for elem in formatted_text:
    elem += ' '
    if 'o ' in elem or 'о ' in elem:            #додав обробку як латиницею так i кирилицею
        outputted_text.append(elem)
print('Кількість слiв, які закінчуються на \'o\' ---> ', len(outputted_text))

