# Напишіть цикл, який буде вимагати від користувача ввести слово, в якому є буква "о"
# (враховуються як великі так і маленькі). Цикл не повинен завершитися, якщо користувач ввів слово без букви "о".

while True:
    inputted_word = input('Введiть одне слово, якe мiстить велику або маленьку лiтеру \'o\' ---> ')
    inputted_word = inputted_word.lower()
    if 'o' in inputted_word or 'о' in inputted_word: #додав обробку як латиницею так i кирилицею
        if ' ' in inputted_word:
            print('Введiть лише ОДНЕ слово без пробiлу')
            continue
        elif not inputted_word.isalpha():
            print('Не використовуйте числа або спецсимволи')
            continue
        else:
            break
print('Дякую, ви неперевершенi!')