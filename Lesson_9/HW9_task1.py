class TransportVehicle:
    '''
    Base Class Description
    '''
    cabin = 'type'
    steering_wheel = True
    driver = 'Human'
    wheels = 0
    def __init__(self, wheels_number, cabin_type):
        self.wheels = wheels_number
        self.cabin = cabin_type
    def start_message(self):
        msg = f'Hello, i\'m a transport and i have {self.wheels} wheels'
        return msg

class Car(TransportVehicle):
    '''
    Sub-Class car Description
    '''
    wheels = 4
    engine = 'diesel'

class Ship(TransportVehicle):
    '''
    Sub-Class ship Description
    '''
    sail = True
    captain = 'Jack Sparrow'
    def captain_greetings(self):
        msg = f'Yo-ho-ho! I\'m a captain {self.captain}!'
        return msg

class Plane(TransportVehicle):
    '''
    Sub-Class plane Description
    '''
    wings = 2
    crew = {'pilot1', 'pilot2', 'mechanic'}
    def __init__(self, new_driver):
        self.driver = new_driver

ferrari = Car(4, 'type1')
victory = Ship(0, 'type2')
fighter_aircraft = Plane('auto-pilot')

print(ferrari.start_message())
print(victory.captain_greetings())
print(fighter_aircraft.driver)