class Point:
    x = None
    y = None

    def __init__(self, x_coord, y_coord):
        self.x_coord = x_coord
        self.y_coord = y_coord

    @property
    def x_coord(self):
        return self.x

    @x_coord.setter
    def x_coord(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        self.x = value

    @property
    def y_coord(self):
        return self.y

    @y_coord.setter
    def y_coord(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        self.y = value


class Line:
    begin = None
    end = None

    def __init__(self, begin_point, end_point):
        self.begin = begin_point
        self.end = end_point

    def length(self):
        k1 = self.begin.x - self.end.x
        k2 = self.begin.y - self.end.y
        res = (k1 ** 2 + k2 ** 2) ** 0.5
        return res

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

    def __lt__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

    def __le__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

    def __gt__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

    def __ge__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError

    def __ne__(self, other):
        if not isinstance(other, type(self)):
            raise TypeError


p1 = Point(1, 2)
p2 = Point(2, 4)
p3 = Point(3, 5)
p4 = Point(4, 6)
line1 = Line(p1, p2)
line2 = Line(p3, p4)
