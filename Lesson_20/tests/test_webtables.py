import pytest

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from Lesson_20.WebTablesPage import WebTablesPage


@pytest.mark.usefixtures('chrome')
@pytest.mark.usefixtures('setup')
class TestWebTablesPage:
    def test_add_new_record(self):
        page = WebTablesPage(self.driver)
        page.open()
        page.click_add_new_record_button()
        page.fill_reg_form_first_name_field('Andrii')
        page.fill_reg_form_last_name_field('Brazhnyk')
        page.fill_reg_form_email_field('brazhnikaa@gmail.com')
        page.fill_reg_form_age_field('29')
        page.fill_reg_form_salary_field('4500')
        page.fill_reg_form_department_field('Quality Engineer')
        page.click_reg_form_submit_button()
