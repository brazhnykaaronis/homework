from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver

page_url = 'https://demoqa.com/webtables'

class WebTablesPage:
    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.url = page_url
        self.add_new_record_button = (By.ID, 'addNewRecordButton')
        self.reg_form_first_name_field = (By.ID, 'firstName')
        self.reg_form_last_name_field = (By.ID, 'lastName')
        self.reg_form_email_field = (By.ID, 'userEmail')
        self.reg_form_age_field = (By.ID, 'age')
        self.reg_form_salary_field = (By.ID, 'salary')
        self.reg_form_department_field = (By.ID, 'department')
        self.reg_form_submit_button = (By.ID, 'submit')


    def open(self) -> 'WebTablesPage':
        self.driver.get(self.url)
        return self

    def click_add_new_record_button(self):
        self.driver.find_element(*self.add_new_record_button).click()

    def clear_reg_form_first_name_field(self) -> None:
        self.driver.find_element(*self.reg_form_first_name_field).clear()

    def fill_reg_form_first_name_field(self, text: str) -> None:
        self.driver.find_element(*self.reg_form_first_name_field).send_keys(text)

    def clear_reg_form_last_name_field(self) -> None:
        self.driver.find_element(*self.reg_form_last_name_field).clear()

    def fill_reg_form_last_name_field(self, text: str) -> None:
        self.driver.find_element(*self.reg_form_last_name_field).send_keys(text)

    def clear_reg_form_email_field(self) -> None:
        self.driver.find_element(*self.reg_form_email_field).clear()

    def fill_reg_form_email_field(self, text: str) -> None:
        self.driver.find_element(*self.reg_form_email_field).send_keys(text)

    def clear_reg_form_age_field(self) -> None:
        self.driver.find_element(*self.reg_form_age_field).clear()

    def fill_reg_form_age_field(self, text: str) -> None:
        self.driver.find_element(*self.reg_form_age_field).send_keys(text)

    def clear_reg_form_salary_field(self) -> None:
        self.driver.find_element(*self.reg_form_salary_field).clear()

    def fill_reg_form_salary_field(self, text: str) -> None:
        self.driver.find_element(*self.reg_form_salary_field).send_keys(text)

    def clear_reg_form_department_field(self) -> None:
        self.driver.find_element(*self.reg_form_department_field).clear()

    def fill_reg_form_department_field(self, text: str) -> None:
        self.driver.find_element(*self.reg_form_department_field).send_keys(text)

    def click_reg_form_submit_button(self):
        self.driver.find_element(*self.reg_form_submit_button).click()