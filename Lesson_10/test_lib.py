# import pytest
# import lib
#
#
# def test1() -> None:
#     """
#     Tests the get_random_string function with a float number
#     """
#     with pytest.raises(TypeError):
#         assert isinstance(lib.get_random_string(1.7), str)
#
#
# def test2() -> None:
#     """
#     Tests the get_random_string function without length
#     """
#     assert isinstance(lib.get_random_string(), str)
#
#
# def test3() -> None:
#     """
#     Tests the get_random_string function with only 1 char
#     """
#     assert isinstance(lib.get_random_string(1), str)
#
#
# def test4() -> None:
#     """
#     Tests the get_random_string function with many chars
#     """
#     assert isinstance(lib.get_random_string(50), str)
#
#
# def test5() -> None:
#     """
#     Tests the get_random_string function with 0 chars
#     """
#     assert isinstance(lib.get_random_string(0), str)
#
#
# def test6() -> None:
#     """
#     Tests the get_random_string function with a negative number
#     """
#     assert lib.get_random_string(-1) == ""
