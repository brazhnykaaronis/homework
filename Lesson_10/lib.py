import random
import string


def get_random_number() -> int:
    """
    Generates a random int between -10^10 and 10^10
    """
    return random.randint(-10 ** 10, 10 ** 10)


def get_random_string(length: int = 10) -> str:
    """
    Generates a random str with the given length
    """
    return ''.join(random.choice(string.ascii_lowercase) for i in range(length))
