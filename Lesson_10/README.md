pip install flake8
pip install mypy
pip install requests
pip install pytest
pip install pytest-mock

pytest -v test_lib.py
pytest -s test_lib.py debug print
pytest -m test_lib.py deselect
pytest -k test_lib.py

flake8 --ignore=E501 lib.py
