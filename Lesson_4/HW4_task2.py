# Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами:
# { "cito": 47.999, "BB_studio": 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324,
# "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "roze-tka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів,
# ціни яких потрапляють в діапазон між мінімальною і максимальною ціною.

# Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"
dict1 = {
    "cito": 47.999,
    "BB_studio": 42.999,
    "momo": 49.999,
    "main-service": 37.245,
    "buy.now": 38.324,
    "x-store": 37.166,
    "the_partner": 38.988,
    "store": 37.720,
    "roze-tka": 38.003
}
lst1 = []
min_value = input('Введiть min ціну -> ')
max_value = input('Введiть max ціну -> ')
for key, value in dict1.items():
    try:
        if float(min_value) <= value <= float(max_value):
            lst1.append(key)
    except ValueError:
        print('Введено невірне значення!')
        break
if lst1 == []:
    print('Підходящих магазинів не знайдено')
else:
    completed_list = ", ".join(lst1)
    print('Вам підійдуть такі магазини ->', completed_list)
