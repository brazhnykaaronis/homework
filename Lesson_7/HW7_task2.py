def stupid_calc(number1, number2, operator):
    """
    Функція приймає два числових аргументи і строковий, який відповідає за операцію між ними
    Args:
        number1: (int) or (float)
        number2: (int) or (float)
        operator: string '+', '-', '/', '*'
    Returns:
        (None) if wrong data
        (int) or (float) if correct data
    """
    if not (type(number1) in (int, float) and type(number2) in (int, float)):
        print('Невірний тип даних')
        return None
    else:
        if len(operator) == 1:
            try:
                if '+' in operator:
                    result = number1 + number2
                elif '-' in operator:
                    result = number1 - number2
                elif '/' in operator:
                    result = number1 / number2
                elif '*' in operator:
                    result = number1 * number2
                print(result)
                return result
            except:
                print('Операція не підтримується')
                return None
        else:
            print('Операція не підтримується')
            return None

stupid_calc(3.2, 5, '+')