def get_date():
    """
    Функція вимагає від користувача ввести дату в форматі "[день].[місяць]" (наприклад "30.08")
    Returns:
        int 1..12 включно
    """
    while True:
        string_date = input('Вкажiть дату у форматi "[день].[місяць]" (наприклад "30.08") ---> ').split('.')
        if len(string_date) != 2 or len(string_date[0]) != 2 or len(string_date[1]) != 2:
            print('Невірний формат даних')
            continue
        try:
            if (int(string_date[0]) == 30 or int(string_date[0]) == 31) and int(string_date[1]) == 2:
                print('Такого дня у лютому нема')
                continue
            if (int(string_date[0]) not in range(1, 32) or int(string_date[1]) not in range(1, 13)):
                print('Невірно вказано день або місяць')
            else:
                return int(string_date[1])
        except:
            print('Введено невірні дані')

def season_definer(month):
    """
    Функція визначає пору року на основі числа яке отримала
    Args:
        month (int):
    Returns:
        String with the type of the season
    """
    try:
        if month in range(3, 6):
            season_type = 'Це весна'
        elif month in range(6, 9):
            season_type = 'Це літо'
        elif month in range(9, 12):
            season_type = 'Це осінь'
        elif month == 1 or month == 2 or month == 12:
            season_type = 'Це зима'
        return season_type
    except:
        print('Недопустиме значення!')

def season():
    """
    Функція виводить на екран назву пори року
    """
    date = get_date()
    season = season_definer(date)
    print(season)

season()