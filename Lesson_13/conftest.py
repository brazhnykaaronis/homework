# import pytest
# from HW13_task1 import MobilePhone
# from HW13_task1 import Laptop
#
#
# @pytest.fixture(scope='class')
# def mobile_phone():
#     mobile_phone_data = MobilePhone(
#         IMEI='111uuu222ooo',
#         screen_resolution='Full HD',
#         brand='Xiaomi',
#         memory_gb=128
#     )
#     yield mobile_phone_data
#
#
# @pytest.fixture(scope='class')
# def laptop():
#     laptop_data = Laptop(
#         IMEI='999999999999999999',
#         screen_resolution='1920x1080',
#         brand='Blackberry',
#         memory_gb=128,
#         is_gaming=False
#     )
#     yield laptop_data
