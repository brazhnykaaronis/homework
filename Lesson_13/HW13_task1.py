# from abc import ABC, abstractmethod
#
#
# class Device(ABC):
#     def __init__(self, IMEI: str, screen_resolution: str, brand: str, memory_gb: int):
#         self.__IMEI = IMEI.upper()
#         self.screen_resolution = screen_resolution.lower()
#         self.brand = brand.upper()
#         self.memory_gb = memory_gb
#
#     def __str__(self):
#         return f'Device from {self.brand} brand, with {self.screen_resolution} screen resolution and {self.memory_gb} GB memory size.'
#
#     @abstractmethod
#     def get_additional_info(self):
#         pass
#
#     @property
#     def IMEI(self):
#         return self.__IMEI
#
#
# class MobilePhone(Device):
#     def __init__(self, IMEI: str, screen_resolution: str, brand: str, memory_gb: int, phone_type: str = 'Smartphone'):
#         super().__init__(brand=brand, screen_resolution=screen_resolution, memory_gb=memory_gb, IMEI=IMEI)
#         self.phone_type = phone_type
#
#     def get_additional_info(self):
#         return f'Type of the device: {self.phone_type}'
#
#
# class Laptop(Device):
#     def __init__(self, IMEI: str, screen_resolution: str, brand: str, memory_gb: int, is_gaming: bool = False):
#         super().__init__(brand=brand, screen_resolution=screen_resolution, memory_gb=memory_gb, IMEI=IMEI)
#         self.is_gaming = is_gaming
#
#     def get_additional_info(self):
#         if self.is_gaming == True:
#             return f'This is a laptop for gaming'
#         else:
#             return f'This is not a gaming laptop'
#
#
# Samsung = MobilePhone('dshfjsdhf3551w', '1920x1080', 'Samsung', 256)
# iPhone = MobilePhone('uurqwoehf21fimxkl3j', '4K', 'Apple', 1024)
# Lenovo = Laptop('qwert1234567', '1080x720', 'Lenovo', 512)
# Macbook = Laptop('7dj3923nc024hc0c4', '2144x1566', 'Apple', 128, is_gaming=True)
