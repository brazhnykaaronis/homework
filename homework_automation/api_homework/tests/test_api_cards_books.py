import requests
import json

url = 'https://deckofcardsapi.com/'


def test_api_get():
    end_point = 'api/deck/new/shuffle'
    response = requests.get(url + end_point)
    data = response.json()
    with open('new_deck.txt', 'w') as file:
        file.write(json.dumps(data))
    assert response.status_code == 200


def test_api_post():
    end_point = 'api/deck/new/shuffle/?deck_count=10'
    response = requests.post(url + end_point)
    data = response.json()
    print(json.dumps(data))
    assert response.status_code == 200


def func_api_post_parametrized(main_url: str, end_point: str, body: dict):
    url_to_use = main_url + end_point
    response = requests.post(url=url_to_use, data=body)
    data = response.json()
    with open('response_file.txt', 'w') as file:
        file.write(json.dumps(data))
    return file


def test_api_parametrized():
    func_api_post_parametrized('https://demoqa.com/', 'Account/v1/User', {"userName": "test_user_for_ap1_homework", "password": "Pa$$w0rd"})
    with open('response_file.txt', 'r') as file:
        text = file.read()
        assert 'User exists!' in text or 'userID' in text
