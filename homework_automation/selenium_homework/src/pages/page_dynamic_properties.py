from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement

from src.helper.elements import get_appeared_element, wait_for_element_to_be_clickable, wait_for_element_change_color


class PageDynamic:
    _instance = None
    URL = 'https://demoqa.com/dynamic-properties'

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, driver: WebDriver):
        self.driver: WebDriver = driver
        self.__button_will_enable_loc = (By.ID, 'enableAfter')
        self.__button_color_change_loc = (By.ID, 'colorChange')
        self.__button_will_visible_loc = (By.ID, 'visibleAfter')

    def open(self) -> 'PageDynamic':
        self.driver.get(self.URL)
        return self

    def button_will_be_enabled(self) -> WebElement:
        button = wait_for_element_to_be_clickable(self.driver, self.__button_will_enable_loc, 6)
        return button

    def button_will_change_color(self) -> WebElement:
        button = wait_for_element_change_color(self.driver, self.__button_color_change_loc, 6)
        return button

    def button_will_be_visible(self) -> WebElement:
        button = get_appeared_element(self.driver, self.__button_will_visible_loc, 6, poll=0.5)
        return button
