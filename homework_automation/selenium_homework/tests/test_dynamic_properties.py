import pytest
from selenium.webdriver.remote.webdriver import WebDriver
from src.pages.page_dynamic_properties import PageDynamic


@pytest.mark.usefixtures('chrome')
class TestDynamicProperties:

    def setup_method(self):
        self.driver: WebDriver = self.driver
        self.page = PageDynamic(self.driver)
        self.page.open()

    def test_button_will_be_enabled(self):
        button = self.page.button_will_be_enabled()
        assert button.is_enabled()

    def test_button_will_change_color(self):
        button = self.page.button_will_change_color()
        assert button

    def test_button_will_be_visible(self):
        button = self.page.button_will_be_visible()
        assert button.is_displayed()
