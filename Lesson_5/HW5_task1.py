# Завдання 1

old_towns = set(input('Назвіть міста (через пробіл) в яких ви побували за минулі 10 років ---> ').lower().split())
new_towns = set(input('Назвіть міста (через пробіл) в які б хотіли поїхати в наступні 10 років ---> ').lower().split())
interception = old_towns.intersection(new_towns)
if not interception:
    print('Ви відкриті для чогось нового!')
else:
    towns = ", ".join(interception).title()
    print('Мабуть, вам дуже сподобалось у цих містах! ---> ', towns)