# Завдання 2
student = {
    'Іван Петров': {
        'Пошта': 'Ivan@gmail.com',
        'Вік': 14,
        'Номер телефону': '+380987771221',
        'Середній бал': 95.8
    },
    'Женя Курич': {
        'Пошта': 'Geka@gmail.com',
        'Вік': 16,
        'Номер телефону': None,
        'Середній бал': 64.5
    },
    'Маша Кера': {
        'Пошта': 'Masha@gmail.com',
        'Вік': 18,
        'Номер телефону': '+380986671221',
        'Середній бал': 80
    },
}
# ваш код нижче
student.update({
    'Андрій Бражник': {
        'Пошта': 'brazhnikaa@gmail.com',
        'Вік': 29,
        'Номер телефону': '+380502193331',
        'Середній бал': 91.2
    }
})
best_students = []
average = []
for person in student:
    if student[person]['Середній бал'] > 90:
        best_students.append(f'{person}: {student[person]["Середній бал"]}')
    average.append(student[person]['Середній бал'])
    if student[person]['Номер телефону'] == None:
        student[person]['Номер телефону'] = '+7777777'
avg_sum = sum(average)
avg_result = avg_sum/len(average)

print('Список студентів у яких середній бал більше 90 ---> ', best_students)
print('Середній бал по групі ---> ', avg_result)