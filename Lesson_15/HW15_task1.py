import requests


def get_currency_rates():
    exchange_rate_url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    response = requests.get(exchange_rate_url)
    if response.status_code == 200:
        if response.headers.get("Content-Type") == "application/json; charset=utf-8":
            try:
                data = response.json()
                return data
            except:
                raise ValueError


def write_currencies_to_txt_file():
    data = get_currency_rates()
    date = data[0]['exchangedate']
    with open("currency_rates.txt", "w") as file:
        file.write(f"[{date}]\n")
        for currency_number, item in enumerate(data, start=1):
            file.write(f"{currency_number}. {item['cc']} to UAH: {item['rate']}\n")


write_currencies_to_txt_file()
