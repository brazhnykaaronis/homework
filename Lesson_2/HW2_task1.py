# Сформуйте стрінг, в якому міститься інформація про певне слово.
# "Word [тут слово] has [тут довжина слова, отримайте з самого слoва] letters",
# наприклад "Word 'Python' has 6 letters".
# Для отримання слова для аналізу скористайтеся константою або функцією input().

print('Please input some word to count the number of letters (do not use digits or special characters)')
inputted_word = input()
if inputted_word.isalpha():
    print('Word \'' + inputted_word + '\' has', len(inputted_word), 'letters')
else:
    print('Please do not use digits or special characters')