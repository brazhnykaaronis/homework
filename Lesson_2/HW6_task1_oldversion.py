# 1. Напишіть функцію, яка приймає два аргументи.
# a. Якщо обидва аргумени відносяться до числових типів функція пермножує ці аргументи і повертає результат
# b. Якшо обидва аргументи відносяться до типу стрінг функція обʼєднує їх в один і повертає
# c. В будь-якому іншому випадку - функція повертає кортеж з двох агрументів
def analyzer():
    argument1 = input('Enter first argument to analyze ---> ')
    argument2 = input('Enter second argument to analyze ---> ')
    try:
        if float(argument1):
            try:
                result = float(argument1) * float(argument2)
                print('Multiplication result is ---> ',result)
                return result
            except:
                print('Your data is ---> ', argument1, argument2)
                return argument1, argument2
    except:
        try:
            if float(argument2):
                print('Your data is ---> ', argument1, argument2)
                return argument1, argument2
        except:
            result = argument1 + argument2
            print('Connection result is ---> ', result)
            return result
analyzer()