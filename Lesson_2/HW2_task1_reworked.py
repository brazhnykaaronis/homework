# Сформуйте стрінг, в якому міститься інформація про певне слово.
# "Word [тут слово] has [тут довжина слова, отримайте з самого слoва] letters",
# наприклад "Word 'Python' has 6 letters".
# Для отримання слова для аналізу скористайтеся константою або функцією input().

# Коментар 1 вiд викладача: чому текст для вводу не проставити відразу в функції інпуту?
# Коментар 2 вiд викладача: користуйся ф-стрічками

inputted_word = input('Please input some word to count the number of letters (do not use digits or special characters) -> ')
result1 = f'Word \'{inputted_word}\' has {len(inputted_word)} letters'
result2 = f'Please do not use digits or special characters'
if inputted_word.isalpha():
#    result1 = f'Word \'{inputted_word}\' has {len(inputted_word)} letters'
    print(result1)
else:
#    result2 = f'Please do not use digits or special characters'
    print(result2)

#test