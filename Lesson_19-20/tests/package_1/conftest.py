import pytest


@pytest.fixture(scope='session', autouse=True)
def package_1_fixture():
    print('\nSESSION Tests are started')
    yield
    print('\nSESSION Tests are finished')