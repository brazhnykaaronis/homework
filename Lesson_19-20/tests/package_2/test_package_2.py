import pytest


@pytest.mark.param
@pytest.mark.parametrize('age', '50')
def test2_1(age):
    pass


@pytest.mark.param
@pytest.mark.parametrize('name, surname', [['Ivan', 'Petrov'], ['Oleg', 'Petrenko'], ['Zhenia', 'Stetsenko']])
def test2_2(name, surname):
    pass


@pytest.mark.param
@pytest.mark.parametrize('car', [['ALfa-Romeo'], ['Bugatti'], ['Ferrari']],
                   ids=['Black', 'Blue', 'Red'])
def test2_3(car):
    pass
