import pytest


class TestsThirdPackage:

    @pytest.mark.pack
    @pytest.mark.for_some_tests
    def test3_1(self):
        pass

    @pytest.mark.pack
    @pytest.mark.rest
    def test3_2(self):
        pass


@pytest.mark.pack
@pytest.mark.for_some_tests
def test3_3():
    pass


@pytest.mark.pack
@pytest.mark.rest
def test3_4():
    pass
