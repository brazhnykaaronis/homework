import pytest
from Lesson_22.pages.CheckBoxPage import CheckBoxPage

expandable_folders_full_list = ['home', 'desktop', 'documents', 'workspace', 'office', 'downloads']


@pytest.mark.usefixtures('chrome')
class TestCheckBoxPage:
    def test_checkboxes(self):
        page = CheckBoxPage(self.driver)
        page.open()
        page.expand_any_folder(*expandable_folders_full_list)
        page.select_checkbox('commands', 'general')
        pass

