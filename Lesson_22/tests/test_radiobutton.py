import pytest
from Lesson_22.pages.RadioButtonPage import RadioButtonPage


@pytest.mark.usefixtures('chrome')
class TestRadioButtonPage:
    def test_activate_yes_radio(self):
        page = RadioButtonPage(self.driver)
        page.open()
        page.select_radio_button('yes')
        assert page.radio_button_is_selected('yes')
        assert page.radio_button_is_selected_2('yes')

    def test_get_radio_buttons_info(self):
        page = RadioButtonPage(self.driver)
        page.open()
        page.get_radio_buttons_info()
        assert len(page.get_radio_buttons_info()) == 9

    def test_activate_disabled_radio_button(self):
        page = RadioButtonPage(self.driver)
        page.open()
        page.force_enable_disabled_button('no')
        page.select_radio_button('no')
        assert page.radio_button_is_selected('no')


