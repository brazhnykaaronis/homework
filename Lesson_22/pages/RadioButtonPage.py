import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from Lesson_22.Widgets.components import Button

page_url = 'https://demoqa.com/radio-button'


@pytest.mark.usefixtures('chrome')
class RadioButtonPage:
    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.url = page_url

    def open(self) -> 'RadioButtonPage':
        self.driver.get(self.url)
        return self

    def select_radio_button(self, name):
        button = self.driver.find_element(By.XPATH, f'//label[contains(@for, "{name}Radio")]')
        Button.click(button)

    def radio_button_is_selected(self, name) -> bool:
        element = self.driver.find_element(By.XPATH, f'//label[contains(@for, "{name}Radio")]//ancestor::div[contains(@class, "radio")]/input')
        return element.is_selected()

    def radio_button_is_selected_2(self, name):
        name = name.title()
        element = self.driver.find_element(By.XPATH, f'//p[contains(@class, "mt-3")]/span[contains(text(), "{name}")]')
        return element

    def get_radio_buttons_info(self):
        radio_buttons_on_the_page = []
        buttons = self.driver.find_elements(By.XPATH, f'//input[contains(@type, "radio")]')
        for button in buttons:
            radio_buttons_on_the_page.append(button.get_attribute("id").replace("Radio", ""))
            if button.is_enabled():
                radio_buttons_on_the_page.append('active button')
            else:
                radio_buttons_on_the_page.append('inactive button')
            if button.is_selected():
                radio_buttons_on_the_page.append('selected')
            else:
                radio_buttons_on_the_page.append('not selected')
        return radio_buttons_on_the_page

    def force_enable_disabled_button(self, name):
        disabled_button = self.driver.find_element(By.ID, "noRadio")
        if not disabled_button.is_enabled():
            self.driver.execute_script("arguments[0].disabled = false;", disabled_button)
        return self
