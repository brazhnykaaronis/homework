import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from Lesson_22.Widgets.components import Button
import configparser

page_url = 'https://demoqa.com/checkbox'
config = configparser.ConfigParser()
config.read('CheckBoxPageLocators.ini')


@pytest.mark.usefixtures('chrome')
class CheckBoxPage:
    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.url = page_url

    def open(self) -> 'CheckBoxPage':
        self.driver.get(self.url)
        return self

    def expand_any_folder(self, *args):
        for item in args:
            button_loc = (self.driver.find_element(By.XPATH, f'//label[contains(@for, "tree-node-{item}")]//ancestor::span/button'))
            Button(button_loc).click()

    def select_checkbox(self, *args):
        for item in args:
            checkbox = self.driver.find_element(By.XPATH, f'//label[contains(@for, "tree-node-{item}")]/span[contains(@class, "checkbox")]')
            Button.click(checkbox)
