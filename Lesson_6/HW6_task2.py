# Візьміть попереднє дз "Касир в кінотеатрі" і перепишіть за допомогою функцій. Памʼятайте про SRP!
def get_user_age():
    while True:
        user_age = input('Вкажіть свій вік ---> ')
        if user_age.isdigit():
            if (int(user_age) > 120 or int(user_age) == 0):
                print('Значення повинно бути вiд 1 до 120 включно!')
            else:
                return user_age
        else:
            print('Будь-ласка, використовуйте лише цифри')

def message_pack(age):
    int_age = int(age)
    if '7' in age:
        msg = 'Вам сьогодні пощастить!'
    elif int_age < 7:
        msg = 'Де твої батьки?'
    elif int_age < 16:
        msg = 'Це фільм для дорослих!'
    elif int_age > 65:
        msg = 'Покажіть пенсійне посвідчення!'
    else:
        msg = 'А білетів вже немає!'
    return msg

def cinema_cashier():
    age = get_user_age()
    answer = message_pack(age)
    print(answer)

cinema_cashier()