# 1. Напишіть функцію, яка приймає два аргументи.
# a. Якщо обидва аргумени відносяться до числових типів функція пермножує ці аргументи і повертає результат
# b. Якшо обидва аргументи відносяться до типу стрінг функція обʼєднує їх в один і повертає
# c. В будь-якому іншому випадку - функція повертає кортеж з двох агрументів
def analyzer(arg1, arg2):
    if type(arg1) in (int, float) and type(arg2) in (int, float):
        result = float(arg1) * float(arg2)
    elif type(arg1) is str and type(arg2) is str:
        result = arg1 + arg2
    else:
        result = (arg1, arg2)
    return result
print(analyzer(True, False))