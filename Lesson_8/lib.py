import time

def decorator(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        wrapped_result = func(*args, **kwargs)
        end = time.time() - start
        print(f'Час виконання функції \'{func}\' склав', end)
        return wrapped_result

    return wrapper

@decorator
def analyzer(arg1, arg2):
    if type(arg1) in (int, float) and type(arg2) in (int, float):
        result = float(arg1) * float(arg2)
    elif type(arg1) is str and type(arg2) is str:
        result = arg1 + arg2
    else:
        result = (arg1, arg2)
    return result

def get_user_age():
    while True:
        user_age = input('Вкажіть свій вік ---> ')
        if user_age.isdigit():
            if (int(user_age) > 120 or int(user_age) == 0):
                print('Значення повинно бути вiд 1 до 120 включно!')
            else:
                return user_age
        else:
            print('Будь-ласка, використовуйте лише цифри')

def message_pack(age):
    int_age = int(age)
    if '7' in age:
        msg = 'Вам сьогодні пощастить!'
    elif int_age < 7:
        msg = 'Де твої батьки?'
    elif int_age < 16:
        msg = 'Це фільм для дорослих!'
    elif int_age > 65:
        msg = 'Покажіть пенсійне посвідчення!'
    else:
        msg = 'А білетів вже немає!'
    return msg

@decorator
def cinema_cashier():
    age = get_user_age()
    answer = message_pack(age)
    print(answer)

def get_date():
    """
    Функція вимагає від користувача ввести дату в форматі "[день].[місяць]" (наприклад "30.08")
    Returns:
        int 1..12 включно
    """
    while True:
        string_date = input('Вкажiть дату у форматi "[день].[місяць]" (наприклад "30.08") ---> ').split('.')
        if len(string_date) != 2 or len(string_date[0]) != 2 or len(string_date[1]) != 2:
            print('Невірний формат даних')
            continue
        try:
            if (int(string_date[0]) == 30 or int(string_date[0]) == 31) and int(string_date[1]) == 2:
                print('Такого дня у лютому нема')
                continue
            if (int(string_date[0]) not in range(1, 32) or int(string_date[1]) not in range(1, 13)):
                print('Невірно вказано день або місяць')
            else:
                return int(string_date[1])
        except:
            print('Введено невірні дані')

def season_definer(month):
    """
    Функція визначає пору року на основі числа яке отримала
    Args:
        month (int):
    Returns:
        String with the type of the season
    """
    try:
        if month in range(3, 6):
            season_type = 'Це весна'
        elif month in range(6, 9):
            season_type = 'Це літо'
        elif month in range(9, 12):
            season_type = 'Це осінь'
        elif month == 1 or month == 2 or month == 12:
            season_type = 'Це зима'
        return season_type
    except:
        print('Недопустиме значення!')

@decorator
def season():
    """
    Функція виводить на екран назву пори року
    """
    date = get_date()
    season = season_definer(date)
    print(season)

@decorator
def stupid_calc(number1, number2, operator):
    """
    Функція приймає два числових аргументи і строковий, який відповідає за операцію між ними
    Args:
        number1: (int) or (float)
        number2: (int) or (float)
        operator: string '+', '-', '/', '*'
    Returns:
        (None) if wrong data
        (int) or (float) if correct data
    """
    if not (type(number1) in (int, float) and type(number2) in (int, float)):
        print('Невірний тип даних')
        return None
    else:
        if len(operator) == 1:
            try:
                if '+' in operator:
                    result = number1 + number2
                elif '-' in operator:
                    result = number1 - number2
                elif '/' in operator:
                    result = number1 / number2
                elif '*' in operator:
                    result = number1 * number2
                print(result)
                return result
            except:
                print('Операція не підтримується')
                return None
        else:
            print('Операція не підтримується')
            return None